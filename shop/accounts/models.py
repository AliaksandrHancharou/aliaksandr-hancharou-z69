from django.db import models
from django.conf import settings


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    photo = models.ImageField(upload_to=f'users/{user}', blank=True)
    date_of_birth = models.DateField(blank=True, null=True)
    biography = models.TextField(blank=True)
    
    def __str__(self) -> str:
        return f'{self.user.username}\'s Profile.'
    
