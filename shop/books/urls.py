from django.urls import path, include
from . import views

app_name = 'books'
urlpatterns = [
    path('', views.index),
    path('<int:book_id>/view', views.book, name='view'),
    path('<int:book_id>/buy', views.buy_book, name='buy'),
]
