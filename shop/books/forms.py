from pyexpat import model
from django import forms
from .models import BuyBook

class BuyBookForm():
    first_name = forms.CharField(label='First Name')
    last_name = forms.CharField(label='Last Name')
    phone_number = forms.IntegerField(label='Phone number')
    city = forms.CharField(label='City')
    street = forms.CharField(label='Street')
    home = forms.CharField(label='Home')
    
    class Meta:
        models = BuyBook
        fields = ('first_name', 'last_name', 'phone_number', 'city', 'street', 'home')