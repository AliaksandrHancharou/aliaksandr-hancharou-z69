from django.contrib import admin
from .models import Book, Author, Genre

class AuthorAdmin(admin.ModelAdmin):
    list_display = ('first_name', 
                    'last_name', 
                    'date_of_birth',
                    'date_of_death')
    list_filter = ('last_name', 'date_of_death')


class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 
                    'description', 
                    'author',
                    'count',
                    'price')
    list_filter = ('price',)

class BuyAdmin(admin.ModelAdmin):
    list_display = ('user', 'first_name', 'last_name', 'phone_number', 'city', 'street', 'home')
    list_filter = ()

admin.site.register(Genre)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
