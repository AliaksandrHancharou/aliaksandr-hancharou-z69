from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from .models import Book, Author, Genre, BuyBook
from .forms import BuyBookForm
from django.contrib.auth.decorators import login_required


def about_shop(req):
    visits = req.session.get('visits', 0)
    books_amount = Book.objects.count()
    author_amount = Author.objects.count()
    return {'books_amount': books_amount,
            'author_amount': author_amount,
            'visits': visits}

def index(request):
    atr_dictionary = about_shop(request)
    request.session['visits'] = request.session.get('visits', 0) + 1
    books = Book.objects.all()
    atr_dictionary['books'] = books
    return render(request, 'index.html', atr_dictionary)

def book(request, book_id):
    atr_dictionary = about_shop(request)
    atr_dictionary['book'] = Book.objects.get(id=book_id)
    return render(request, 'book.html', atr_dictionary)

def buy_book(request, book_id):
    book = Book.objects.get(id=book_id)
    if request.method == 'POST':
        buy_form = BuyBookForm(request.POST, user=request.user)
        if buy_form.is_valid():
            order = buy_form.save(commit=False)
            order.save()
            return HttpResponseRedirect(reverse('index'))
        return render(request, 'buy.html', {'book': book, 'buy_form': buy_form})
    return render(request, 'buy.html', {'book': book, 'buy_form': BuyBookForm()})



    #         BuyBook.objects.create(BuyBook=order)
            
    # atr_dictionary = about_shop(request)
    # atr_dictionary['book'] = Book.objects.get(id=book_id)
    # return render(request, 'buy_book.html', atr_dictionary)