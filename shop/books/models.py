from django.db import models
from django.urls import reverse
from django.conf import settings

class Genre(models.Model):
    name = models.CharField(max_length=100, help_text="Enter genre name")

    def __str__(self):
        return self.name


class Author(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_of_birth = models.DateField(null=False)
    date_of_death = models.DateField(null=True, blank=True)

    def __str__(self):
        return f"{self.last_name} {self.first_name}"


class Book(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(help_text="Enter short description")
    genre = models.ManyToManyField(Genre, help_text="Select a genre")
    author = models.ForeignKey('Author', on_delete=models.SET_NULL, null=True)
    count = models.IntegerField(default=0)
    price = models.FloatField()

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('books:view', args=[str(self.id)])
    
    def genres(self):
        return ','.join([genre.name for genre in self.genre.all()])

    
class BuyBook(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    phone_number = models.IntegerField()
    city = models.CharField(max_length=255)
    street = models.CharField(max_length=255)
    home = models.CharField(max_length=255)
    
    def __str__(self) -> str:
        return f'{self.first_name} {self.last_name}\'s order'